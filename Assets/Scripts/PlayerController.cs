﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	// will be exposed to Inspector 
	public float speed;
	public GUIText countText;
	public GUIText winText;
	private int count; // of pickups


	void Start()
	{
		count = 0;
		winText.text = "";
		SetCountText();
	}

	void FixedUpdate()
	{//called before physics applied

		// The "Input" class provides access to the InputManager

		// we are going to use these inputs to apply forces to the Rigidbody component
		// in order to move the Player on the screen
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		// we have access to rigidbody because it is a component on Player.
		// we have to add a multiplier because the movement is much too slow otherwise.
		rigidbody.AddForce (movement * speed * Time.deltaTime);
	}

	//reference to the trigger collider 
	// we need to make sure that the "other" object "is a" trigger (See 'Box Collider' component of PickUp prefab in Inspector
	// and click 'Is Trigger'
	void OnTriggerEnter(Collider other)
	{
		//Destroy(other.gameObject); // all children and their components removed from scene

		//Deactivate pickup objects - by string tag ...yuck.
		if (other.gameObject.tag == "PickUp") {
			count++;
			SetCountText();
			other.gameObject.SetActive (false);
		}
	}

	void SetCountText(){
		countText.text = "Count: " + count.ToString();
		if (count >= 17) {
			winText.text = "You win!";
		}
	}

	//Note from tutorial:
	// Very important so that we don't waste resources. Any GameObject w/a Collider AND Rigidbody is considered dynamic and Unity 
	// will not cache the collider volumes of those GameObjects (i.e. the PickUp cubes, which are constantly moving). 
	// Otherwise, if the object has a Collider but no Rigidbody, it's considered static.

	// Kinematic Rigidbody objects do not respond to physics forces . Can be animated and/or moved by transform.
}
