﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Not using forces, so we can use this
	void Update () {
		//change the transform values every frame
		// rotate the transform
		transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime); //smooth and frame rate independent
	}
}
