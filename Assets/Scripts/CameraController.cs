﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject player; // drag and drop in Unity to match up the Player GameObject instance in the Hierarchy to the "Player" slot
	// in MainCamera's component (variable Player) in Inspector tab
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position;
	}
	 
	void LateUpdate () {
		transform.position = player.transform.position + offset;
	}
}
